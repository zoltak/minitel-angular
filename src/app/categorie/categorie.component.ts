import { Component, OnInit } from '@angular/core';
import { Categorie } from '../models/categorie.model';
import { CategorieService } from '../services/categorie.service';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {

  listeCategories : Categorie[];

  constructor(private categorieService: CategorieService) { }

  ngOnInit()
  {
    this.categorieService.getCategories().subscribe( data => { this.listeCategories = data; } )
  }

}
