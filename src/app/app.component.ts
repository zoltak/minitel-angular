import { Component, OnInit, Input } from '@angular/core';
import { Article } from './models/article.model';
import { ArticleService } from './services/article.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  articleSelected : Article;
  affichageDeLaBalise : boolean = false;

  constructor(){ }
  ngOnInit(){ }

  majDetailArticle(event){
    this.articleSelected = event;
  }

}
