import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Categorie } from '../models/categorie.model';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  baseUrl: string = 'http://localhost:8080/minitel-api/categorie/categories';
  httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'}) };

  constructor(private http: HttpClient)
  {

  }

  getCategories()
  {
      return this.http.get<Categorie[]>(this.baseUrl, this.httpOptions).pipe(
          map(
              (jsonArray: Object[]) => jsonArray.map(jsonItem => Categorie.fromJson(jsonItem))
          )
      );
  }
}
