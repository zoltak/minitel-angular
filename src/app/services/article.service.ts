import { Article } from '../models/article.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ArticleService
{
    baseUrl: string = 'http://localhost:8080/minitel-api/article';
    httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'}) };

    constructor(private http: HttpClient)
    {

    }

    getArticles()
    {
        return this.http.get<Article[]>(this.baseUrl + '/articles', this.httpOptions).pipe(
            map(
                (jsonArray: Object[]) => jsonArray.map(jsonItem => Article.fromJson(jsonItem))
            )
        );
    }

    
}