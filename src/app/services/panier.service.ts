import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Panier } from '../models/panier.model';
import { map } from 'rxjs/operators';
import { Article } from '../models/article.model';

@Injectable({
  providedIn: 'root'
})
export class PanierService {

  baseUrl: string = 'http://localhost:8080/minitel-api/panier';
  httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'}) };

  constructor(private http: HttpClient) { }

  getPanier(idPanier: number) {
    return this.http.get<Panier>(this.baseUrl + `/${idPanier}`, this.httpOptions).pipe(
      map(
        jsonItem => Panier.fromJson(jsonItem)
    )
    );
  }

  ajouterArticleDansPanier(article: Article) {
    this.httpOptions.headers.append('Access-Control-Allow-Origin', 'http://localhost:8080');
    this.httpOptions.headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    this.httpOptions.headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    this.httpOptions.headers.append('Access-Control-Allow-Credentials', 'true');
    this.http.put(this.baseUrl + `/2/${article.id}`, null, this.httpOptions).subscribe();
    
    console.log("ajouterArticleDans Panier() dans PanierService");
  }

  supprimerArticleDansPanier(article: Article) {
    this.http.delete(this.baseUrl + `/2/${article.id}`, this.httpOptions).subscribe();
  }

  clearPanier() {
    this.http.delete(this.baseUrl + `/2/`, this.httpOptions).subscribe();
  }
  majDesStock() {
    this.http.put(this.baseUrl + `/2/`, null, this.httpOptions).subscribe();
  }
}
