import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ArticleService } from '../services/article.service';
import { Article } from '../models/article.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  listeArticles : Article[];
  @Output() articleSelected = new EventEmitter<Article>();

  constructor(private articleService: ArticleService) {  }

  ngOnInit() {
    this.articleService.getArticles().subscribe( data => { this.listeArticles = data; } )
  }

  onSelect(article: Article)
  {
    console.log('onSelect()'+article.id);
    this.articleSelected.emit(article);
    
  }

}
