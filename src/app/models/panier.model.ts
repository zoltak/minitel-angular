import { Article } from './article.model';

export class Panier
{
    constructor(
        public id?: number,
        public articles?: Article[]
    ) {

    }
    public static fromJson(json: Object): Panier {

        return new Panier(
            json['id'],
            json['articles']
        );
    }
}