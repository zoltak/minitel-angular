import { Constructeur } from './constructeur.model';
import { Categorie } from './categorie.model';

export class Article
{
    

    constructor(
        public id: number,
         public designation: string,
          public categorie: Categorie,
           public image: string,
            public prix: number,
             public content: string,
              public constructeur: Constructeur,
               public dateMiseEnligne: Date,
                public stock: number
                )
        {
            
        }
    
    public static fromJson(json: Object): Article
    {
        return new Article(
            json['id'],
            json['designation'],
            json['categorie'],
            json['image'],
            json['prix'],
            json['content'],
            json['constructeur'],
            new Date(json['dateMiseEnligne']),
            json['stock']
            );
    }

}

