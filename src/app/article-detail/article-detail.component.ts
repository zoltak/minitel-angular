import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../models/article.model';
import { PanierService } from '../services/panier.service';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss']
})
export class ArticleDetailComponent implements OnInit
{
  @Input() articleDetail: Article;

  constructor(private panierService: PanierService) { }

  ngOnInit() {
    if (this.articleDetail != null)
    console.log("ArticleDetailComponent : " + this.articleDetail.id);
   }

   ajouterArticleDansPanier()
   {
      this.panierService.ajouterArticleDansPanier(this.articleDetail);
      console.log("ajouterArticleDans Panier() dans ArticleDetailComponent");
   }

}
