import { Component, OnInit, Input } from '@angular/core';
import { PanierService } from '../services/panier.service';
import { Panier } from '../models/panier.model';
import { Article } from '../models/article.model';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  panier : Panier = new Panier();
  prixPanier: number;
  nombreArticlesPanier: number;

  constructor(private panierService : PanierService) { }

  ngOnInit() {
    this.panierService.getPanier(2).subscribe(
      data => {
        this.panier = data;
        this.calculPrixTotal();
      }
    );
    
  }

  calculPrixTotal(): void {
    this.nombreArticlesPanier = this.panier.articles.length;
    this.prixPanier = 0;
    for (let i = 0; i < this.nombreArticlesPanier; i++ ) {
      this.prixPanier += this.panier.articles[i].prix;
    }
  }

  supprimer(article) {
    this.panierService.supprimerArticleDansPanier(article);
  }

  clearPanierEtMajDesStock() {
    this.panierService.majDesStock();
    this.panierService.clearPanier();
    
  }
}
